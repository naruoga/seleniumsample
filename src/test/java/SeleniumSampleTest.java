import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Created by ogasawara-pc on 2016/07/04.
 */
public class SeleniumSampleTest {
    protected WebDriver driver;

    @Before
    public void setup() {
        driver = new FirefoxDriver();
    }

    @Test
    public void seleniumSampleTest() {
        driver.get("http://www.shiftinc.jp");
        try {
            assertThat(driver.getTitle(), is("株式会社SHIFT ｜TOP"));
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @After
    public void tearDown() {
        driver.quit();
    }
}
